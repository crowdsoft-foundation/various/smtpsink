FROM python:3.9-slim

ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get install nano build-essential iputils-ping -y && apt-get clean
RUN pip install --upgrade pip

COPY src/requirements.txt /src/app/requirements.txt
RUN pip3 install -r /src/app/requirements.txt

COPY src /src

RUN chmod 777 /src/runApp.sh
RUN chmod +x /src/runApp.sh

EXPOSE 10025

CMD ["/src/runApp.sh"]
